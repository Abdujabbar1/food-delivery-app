package com.example.food_delivery_app.entity.role;

public enum RoleEnum {
    SUPER_ADMIN,
    ADMIN,
    CUSTOMER,
    DELIVERY,
    OTHER
}
