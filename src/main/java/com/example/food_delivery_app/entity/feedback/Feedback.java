package com.example.food_delivery_app.entity.feedback;

import com.example.food_delivery_app.entity.order.Order;
import com.example.food_delivery_app.common.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "feedbacks")
public class Feedback extends AbsEntity {

    private String text;
    private short rate;
    @ManyToOne
    private Order order;
}
