package com.example.food_delivery_app.entity.verificationCode;

import com.example.food_delivery_app.common.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.time.LocalTime;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "verification_codes")
public class VerificationCodes extends AbsEntity {
    private String phoneNumber;
    private long smsCode;
    private LocalTime expireAt;
}
