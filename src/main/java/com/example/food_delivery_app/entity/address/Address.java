package com.example.food_delivery_app.entity.address;

import com.example.food_delivery_app.entity.user.User;
import com.example.food_delivery_app.common.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "addresses")
public class Address extends AbsEntity {
    private String name;
    private String landMark;
    private long houseNumber;
    private long entrance;
    private long floor;
    private long flat;
    private float latitude;
    private float longitude;

    @ManyToOne
    private User user;
    @ManyToOne
    private District district;
}
