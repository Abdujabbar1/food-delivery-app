package com.example.food_delivery_app.entity.address;

import com.example.food_delivery_app.common.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "districts")
public class District extends AbsEntity {
    private String name;
}
