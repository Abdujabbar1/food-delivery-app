package com.example.food_delivery_app.entity.order;

import com.example.food_delivery_app.entity.food.Food;
import com.example.food_delivery_app.common.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "orders_items")
public class OrderItem extends AbsEntity {
    @ManyToOne
    private Food food;
    @ManyToOne
    private Order order;
    public long quantity;
}
