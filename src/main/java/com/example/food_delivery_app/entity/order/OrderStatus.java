package com.example.food_delivery_app.entity.order;

public enum OrderStatus {
    PENDING,
    ACCEPTED,
    CANCELLED,
    PREPARING,
    READY,
    ON_THE_WAY,
    COMPLETED
}
