package com.example.food_delivery_app.entity.order;

import com.example.food_delivery_app.entity.user.User;
import com.example.food_delivery_app.common.AbsEntity;
import com.example.food_delivery_app.entity.address.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.time.LocalTime;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "orders")
public class Order extends AbsEntity {
    @ManyToOne
    private User user;
    @ManyToOne
    private User delivery;
    private long orderNumber;
    private LocalTime orderAt;
    private LocalTime estimatedTime;
    @Enumerated
    private OrderStatus status;
    private double totalSum;
    @OneToOne
    private Address address;
    private double deliveryPrice;
}
