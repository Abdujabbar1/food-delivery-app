package com.example.food_delivery_app.entity.attachment;

import com.example.food_delivery_app.common.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "attachment_content")
@OnDelete(action = OnDeleteAction.CASCADE)
public class AttachmentContent extends AbsEntity {
    private byte[] bytes;

    @OneToOne(cascade = CascadeType.MERGE)
    private Attachment attachment;
}
