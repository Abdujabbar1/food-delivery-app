package com.example.food_delivery_app.entity.attachment;

import com.example.food_delivery_app.common.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "attachments")
@OnDelete(action = OnDeleteAction.CASCADE)
public class Attachment extends AbsEntity {
    private String originalName;
    private String contentType;
    private long size;
}
