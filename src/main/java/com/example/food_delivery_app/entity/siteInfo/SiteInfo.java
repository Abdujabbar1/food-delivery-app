package com.example.food_delivery_app.entity.siteInfo;

import com.example.food_delivery_app.common.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "site_info")
public class SiteInfo extends AbsEntity {
    private String address;
    private String phoneNumber;
    private float lat;
    private float along;
}
