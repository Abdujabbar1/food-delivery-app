package com.example.food_delivery_app.entity.paytype;

public enum PayType {
    CLICK,
    PAYME,
    CARD,
    CASH
}
