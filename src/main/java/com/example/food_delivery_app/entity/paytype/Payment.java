package com.example.food_delivery_app.entity.paytype;

import com.example.food_delivery_app.entity.order.Order;
import com.example.food_delivery_app.common.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "payments")
public class Payment extends AbsEntity {
    @Enumerated
    private PayType payType;
    public double totalAmount;
    @ManyToOne
    private Order order;
}
