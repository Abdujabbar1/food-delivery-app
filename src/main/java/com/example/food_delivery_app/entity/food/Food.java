package com.example.food_delivery_app.entity.food;

import com.example.food_delivery_app.entity.category.Category;
import com.example.food_delivery_app.common.AbsEntity;
import com.example.food_delivery_app.entity.attachment.Attachment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalTime;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "foods")
public class Food extends AbsEntity {
    @Column(nullable = false, unique = true)
    private String name;
    private String description;
    @Column(nullable = false)
    private double price;
    private boolean isAvailable;
//    private LocalTime availableFrom;
//    private LocalTime availableTo;
//    private String preparationTime;

    @ManyToOne
    private Attachment image;
    @ManyToOne
    private Category category;
}
