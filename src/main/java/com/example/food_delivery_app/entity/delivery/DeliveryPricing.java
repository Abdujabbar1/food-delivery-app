package com.example.food_delivery_app.entity.delivery;

import com.example.food_delivery_app.common.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "deliveries_prices")
public class DeliveryPricing extends AbsEntity {
    double initial_delivery_price;
    double delivery_price_per_km;
}
