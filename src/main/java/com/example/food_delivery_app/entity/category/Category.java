package com.example.food_delivery_app.entity.category;

import com.example.food_delivery_app.common.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "categories")
public class Category extends AbsEntity {
    private String nameUz;
    private String nameRu;
    private String nameEn;
    private String descriptionUz;
}
