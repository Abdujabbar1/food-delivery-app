package com.example.food_delivery_app.entity.user;


import com.example.food_delivery_app.entity.permission.Permission;
import com.example.food_delivery_app.entity.role.Role;
import com.example.food_delivery_app.common.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "users")
public class User extends AbsEntity implements UserDetails {

    private String fullName;
    @Column(nullable = false, unique = true)
    private String phoneNumber;
    private String smsCode;
    private boolean isBlocked;

    @ManyToMany
    private Set<Role> roleList;
    @ManyToMany
    private Set<Permission> permissionList;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (Role role : roleList) {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getRoleEnum().name()));
        }
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return this.smsCode;
    }

    @Override
    public String getUsername() {
        return this.phoneNumber;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public User(String fullName, String phoneNumber, boolean isBlocked) {
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
        this.isBlocked = isBlocked;
    }
}
