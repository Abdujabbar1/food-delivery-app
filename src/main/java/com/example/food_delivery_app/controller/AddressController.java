package com.example.food_delivery_app.controller;

import com.example.food_delivery_app.dtos.AddressDto;
import com.example.food_delivery_app.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("${app.domain}"+"/address")
@RequiredArgsConstructor
public class AddressController {

    private final AddressService addressService;

    @GetMapping
    public HttpEntity<?> getAllAddress() {
        return addressService.getAllAddresses();
    }

    @GetMapping("/{addressId}")
    public HttpEntity<?> getAddressById(@PathVariable UUID addressId){
        return addressService.getAddressById(addressId);
    }

    @PostMapping("/add")
    public HttpEntity<?> addAddress(@RequestBody AddressDto addressDto){
        return addressService.addAddress(addressDto);
    }

    @PutMapping("/edit/{addressId}")
    public HttpEntity<?> editAddress(@PathVariable UUID addressId, @RequestBody AddressDto addressDto){
        return addressService.editAddress(addressId, addressDto);
    }

    @DeleteMapping("/delete/{addressId}")
    public HttpEntity<?> deleteAddress(@PathVariable UUID addressId){
        return addressService.deleteAddress(addressId);
    }
}
