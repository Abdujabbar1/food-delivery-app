package com.example.food_delivery_app.controller;

import com.example.food_delivery_app.dtos.DistrictDto;
import com.example.food_delivery_app.service.DistrictService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("${app.domain}"+"/district")
@RequiredArgsConstructor
public class DistrictController {
    private final DistrictService districtService;

    @GetMapping()
    public HttpEntity<?> getAllDistrict(){
        return districtService.getAllDistrict();
    }

    @GetMapping("/{districtId}")
    public HttpEntity<?> getDistrictById(@PathVariable UUID districtId){
        return districtService.getDistrictById(districtId);
    }

    @PostMapping("/add")
    public HttpEntity<?> addDistrict(@RequestBody DistrictDto districtDto){
        return districtService.addDistrict(districtDto);
    }

    @PutMapping("/edit/{districtId}")
    public HttpEntity<?> editDistrict(@PathVariable UUID districtId, @RequestBody DistrictDto districtDto){
        return districtService.editDistrict(districtId, districtDto);
    }

    @DeleteMapping("/delete/{districtId}")
    public HttpEntity<?> deleteDistrict(@PathVariable UUID districtId){
        return districtService.deleteDistrict(districtId);
    }
}
