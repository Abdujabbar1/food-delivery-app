package com.example.food_delivery_app.controller;

import com.example.food_delivery_app.service.AttachmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import java.util.UUID;

@RestController
@RequestMapping("${app.domain}"+"/attachment")
@RequiredArgsConstructor
public class AttachmentController {
    private final AttachmentService attachmentService;

    @PostMapping("/upload")
    public HttpEntity<?> fileUpload(MultipartFile file){
       return attachmentService.fileUpload(file);
    }

    @DeleteMapping("/delete-file/{fileId}")
    public HttpEntity<?> deleteFile(@PathVariable UUID fileId){
        return attachmentService.deleteFile(fileId);
    }
}
