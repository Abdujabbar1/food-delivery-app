package com.example.food_delivery_app.controller;

import com.example.food_delivery_app.dtos.FoodDto;
import com.example.food_delivery_app.service.FoodService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("${app.domain}"+"/food")
@RequiredArgsConstructor
public class FoodController {

    private final FoodService foodService;

    @GetMapping
    public HttpEntity<?> getAllFoods() {
        return foodService.getAllFood();
    }

    @GetMapping("/{foodId}")
    public HttpEntity<?> getFoodById(@PathVariable UUID foodId) {
        return foodService.getFoodById(foodId);
    }

    @PostMapping("/add")
    public HttpEntity<?> addFood(@RequestBody FoodDto foodDto) {
        return foodService.addFood(foodDto);
    }

    @PutMapping("/edit/{foodId}")
    public HttpEntity<?> editFood(@PathVariable UUID foodId, @RequestBody FoodDto foodDto) {
        return foodService.editFood(foodId, foodDto);
    }

    @DeleteMapping("/delete/{foodId}")
    public HttpEntity<?> deleteFood(@PathVariable UUID foodId) {
        return foodService.deleteFood(foodId);
    }
}
