package com.example.food_delivery_app.common;

import com.example.food_delivery_app.dtos.FoodDto;
import com.example.food_delivery_app.entity.address.Address;
import com.example.food_delivery_app.entity.address.District;
import com.example.food_delivery_app.entity.attachment.Attachment;
import com.example.food_delivery_app.entity.category.Category;
import com.example.food_delivery_app.entity.food.Food;
import com.example.food_delivery_app.entity.permission.Permission;
import com.example.food_delivery_app.entity.role.Role;
import com.example.food_delivery_app.entity.role.RoleEnum;
import com.example.food_delivery_app.entity.user.User;
import com.example.food_delivery_app.repository.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.util.*;

@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.sql.init.mode}")
    String initMode;

    final AddressRepo addressRepo;
    final AttachmentRepo attachmentRepo;
    final AttachmentContentRepo contentRepo;
    final CategoryRepo categoryRepo;
    final DistrictRepo districtRepo;
    final DeliveryPricingRepo deliveryPricingRepo;
    final FeedbackRepo feedbackRepo;
    final FoodRepo foodRepo;
    final OrderRepo orderRepo;
    final OrderItemRepo orderItemRepo;
    final PaymentsRepo paymentsRepo;
    final PermissionRepo permissionRepo;
    final RoleRepo roleRepo;
    final SiteInfoRepo siteInfoRepo;
    final UserRepo userRepo;
    final VerificationCodeRepo verificationCodeRepo;

    public DataLoader(AddressRepo addressRepo,
                      AttachmentRepo attachmentRepo,
                      AttachmentContentRepo contentRepo,
                      CategoryRepo categoryRepo,
                      DistrictRepo districtRepo,
                      DeliveryPricingRepo deliveryPricingRepo,
                      FeedbackRepo feedbackRepo,
                      FoodRepo foodRepo,
                      OrderRepo orderRepo,
                      OrderItemRepo orderItemRepo,
                      PaymentsRepo paymentsRepo,
                      PermissionRepo permissionRepo,
                      RoleRepo roleRepo,
                      SiteInfoRepo siteInfoRepo,
                      UserRepo userRepo,
                      VerificationCodeRepo verificationCodeRepo) {

        this.addressRepo = addressRepo;
        this.attachmentRepo = attachmentRepo;
        this.contentRepo = contentRepo;
        this.categoryRepo = categoryRepo;
        this.districtRepo = districtRepo;
        this.deliveryPricingRepo = deliveryPricingRepo;
        this.feedbackRepo = feedbackRepo;
        this.foodRepo = foodRepo;
        this.orderRepo = orderRepo;
        this.orderItemRepo = orderItemRepo;
        this.paymentsRepo = paymentsRepo;
        this.permissionRepo = permissionRepo;
        this.roleRepo = roleRepo;
        this.siteInfoRepo = siteInfoRepo;
        this.userRepo = userRepo;
        this.verificationCodeRepo = verificationCodeRepo;
    }

    @Override
    public void run(String... args) throws Exception {
        if (initMode.equals("always")) {


            User user = new User("Ali", "+998900000009", false);
            User delivery = new User("Murod", "+998901234567", false);

            Attachment image = new Attachment("image", "image.jpg", 100000L);

            Category category = new Category("Sendvich", "Сендвич", "Sandwich", "Yeguliklar");

            Food food = new Food("Burger",
                    "Ajoyib",
                    18000,
                    true,
                    image,
                    category);

            Food food1 = new Food("Lavash",
                    "Nice",
                    20000,
                    true,
                    image,
                    category);

            District district = new District("Chilanzar");

            Address address = new Address("Name",
                    "Toshkent, Chilanzar",
                    96,
                    3, 2, 1, 41.263593F, 69.217930F, user, district);


            userRepo.save(user);
            attachmentRepo.save(image);
            categoryRepo.save(category);
            foodRepo.save(food);
            districtRepo.save(district);
            addressRepo.save(address);
        }

    }
}
