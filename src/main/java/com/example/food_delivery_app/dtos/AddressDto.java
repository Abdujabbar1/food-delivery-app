package com.example.food_delivery_app.dtos;

import com.example.food_delivery_app.entity.address.District;
import com.example.food_delivery_app.entity.user.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.ManyToOne;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AddressDto {
    private String name;
    private String landMark;
    private long houseNumber;
    private long entrance;
    private long floor;
    private long flat;
    private float latitude;
    private float longitude;
    private UUID userId;
    private UUID districtId;
}
