package com.example.food_delivery_app.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserDto {

    private String fullName;
    private String phoneNumber;
    private boolean isBlocked;

}
