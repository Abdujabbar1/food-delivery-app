package com.example.food_delivery_app.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FoodDto {
    private String name;
    private String description;
    private double price;
    private boolean available;
//    private LocalTime availableFrom;
//    private LocalTime availableTo;
//    private String preparationTime;
    private UUID imageId;
    private UUID categoryId;
}
