package com.example.food_delivery_app.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrdersDto {
    private UUID userId;
    private UUID deliveryId;
    private long order_number;
    private LocalTime orderAt;
    private LocalTime estimatedTime;
    private String status;
    private double totalSum;
    private UUID addressId;
    private double deliveryPrice;
}
