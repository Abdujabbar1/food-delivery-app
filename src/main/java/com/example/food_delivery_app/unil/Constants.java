package com.example.food_delivery_app.unil;

public interface Constants {

    String DEFAULT_PAGE_NUMBER = "1";
    String DEFAULT_PAGE_SIZE = "10";
}
