package com.example.food_delivery_app.service;

import com.example.food_delivery_app.dtos.AddressDto;
import com.example.food_delivery_app.entity.address.Address;
import com.example.food_delivery_app.common.ApiResponse;
import com.example.food_delivery_app.entity.address.District;
import com.example.food_delivery_app.entity.user.User;
import com.example.food_delivery_app.repository.AddressRepo;
import com.example.food_delivery_app.repository.DistrictRepo;
import com.example.food_delivery_app.repository.UserRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AddressService {

    private final AddressRepo addressRepo;
    private final DistrictRepo districtRepo;
    private final UserRepo userRepo;

    public HttpEntity<?> getAllAddresses() {
        List<Address> addressList = findAllAddress();
        if (addressList.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("Address not found!", false), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new ApiResponse("Addresses found!", true, addressList), HttpStatus.OK);
    }

    public HttpEntity<?> getAddressById(UUID addressId) {
        Optional<Address> addressById = findAddressById(addressId);
        if (addressById.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("Address not found!", false), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new ApiResponse("Address found!", true, addressById.get()), HttpStatus.OK);
    }

    public HttpEntity<?> addAddress(AddressDto addressDto) {
        Optional<User> userById = findUserById(addressDto.getUserId());
        if (userById.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("User not found!", false), HttpStatus.NOT_FOUND);
        }
        Optional<District> districtById = findDistrictById(addressDto.getDistrictId());
        if (districtById.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("District not found!", false), HttpStatus.NOT_FOUND);
        }
        try {
            Address newAddress = new Address(addressDto.getName(),
                    addressDto.getLandMark(),
                    addressDto.getHouseNumber(),
                    addressDto.getEntrance(),
                    addressDto.getFloor(),
                    addressDto.getFlat(),
                    addressDto.getLatitude(),
                    addressDto.getLongitude(),
                    userById.get(),
                    districtById.get());

            saveAddress(newAddress);

            return new ResponseEntity<>(new ApiResponse("Address success added!", true, newAddress), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ApiResponse("An error occurred while adding the address!", false), HttpStatus.EXPECTATION_FAILED);
        }
    }

    public HttpEntity<?> editAddress(UUID addressId, AddressDto addressDto){
        Optional<Address> addressById = findAddressById(addressId);
        if (addressById.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("Address not found!", false), HttpStatus.NOT_FOUND);
        }
        Optional<District> districtById = findDistrictById(addressDto.getDistrictId());
        if (districtById.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("District not found!", false), HttpStatus.NOT_FOUND);
        }
        Optional<User> userById = findUserById(addressDto.getUserId());
        if (userById.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("User not found!", false), HttpStatus.NOT_FOUND);
        }
        try {
            Address editAddress = addressById.get();

            editAddress.setName(addressDto.getName());
            editAddress.setLandMark(addressDto.getLandMark());
            editAddress.setHouseNumber(addressDto.getHouseNumber());
            editAddress.setEntrance(addressDto.getEntrance());
            editAddress.setFloor(addressDto.getFloor());
            editAddress.setFlat(addressDto.getFlat());
            editAddress.setLatitude(addressDto.getLatitude());
            editAddress.setLongitude(addressDto.getLongitude());
            editAddress.setDistrict(districtById.get());
            editAddress.setUser(userById.get());

            saveAddress(editAddress);

            return new ResponseEntity<>(new ApiResponse("Address success edited!", true, editAddress), HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(new ApiResponse("An error occurred while editing the address!", false), HttpStatus.EXPECTATION_FAILED);
        }
    }

    public HttpEntity<?> deleteAddress(UUID addressId){
        Optional<Address> addressById = findAddressById(addressId);
        if (addressById.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("Address not found!", false), HttpStatus.NOT_FOUND);
        }
        try {
            addressRepo.delete(addressById.get());

            return new ResponseEntity<>(new ApiResponse("Address success delete!", true), HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(new ApiResponse("An error occurred while deleting the address!", false), HttpStatus.EXPECTATION_FAILED);
        }
    }

    private void saveAddress(Address address){
        addressRepo.save(address);
    }

    private Optional<District> findDistrictById(UUID districtId) {
        return districtRepo.findById(districtId);
    }

    private Optional<User> findUserById(UUID userId) {
        return userRepo.findById(userId);
    }

    private List<Address> findAllAddress() {
        return addressRepo.findAll();
    }

    private Optional<Address> findAddressById(UUID addressId) {
        return addressRepo.findById(addressId);
    }
}
