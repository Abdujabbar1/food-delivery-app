package com.example.food_delivery_app.service;

import com.example.food_delivery_app.dtos.FoodDto;
import com.example.food_delivery_app.entity.attachment.Attachment;
import com.example.food_delivery_app.entity.category.Category;
import com.example.food_delivery_app.entity.food.Food;
import com.example.food_delivery_app.common.ApiResponse;
import com.example.food_delivery_app.projection.FoodView;
import com.example.food_delivery_app.repository.AttachmentRepo;
import com.example.food_delivery_app.repository.CategoryRepo;
import com.example.food_delivery_app.repository.FoodRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FoodService {

    private final FoodRepo foodRepo;
    private final AttachmentRepo attachmentRepo;
    private final CategoryRepo categoryRepo;

    public HttpEntity<?> getAllFood() {
        List<FoodView> allFood = foodRepo.getAllFoods();
        if (allFood.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("Product not found!", false), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new ApiResponse("All product!", true, allFood), HttpStatus.OK);
    }

    public HttpEntity<?> getFoodById(UUID foodId) {
        FoodView foodById = foodRepo.getFoodById(foodId);
        if (foodById == null) {
            return new ResponseEntity<>(new ApiResponse("Product not found!", false), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new ApiResponse("Product found!", true, foodById), HttpStatus.OK);
    }

    public HttpEntity<?> addFood(FoodDto foodDto) {
        boolean byName = foodRepo.existsByName(foodDto.getName());
        if (byName) {
            return new ResponseEntity<>(new ApiResponse("A product with that name already exists!", false), HttpStatus.ALREADY_REPORTED);
        }
        Optional<Attachment> imageOptional = attachmentRepo.findById(foodDto.getImageId());
        if (imageOptional.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("Image not found!", false), HttpStatus.NOT_FOUND);
        }
        Optional<Category> categoryOptional = categoryRepo.findById(foodDto.getCategoryId());
        if (categoryOptional.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("Category not found!", false), HttpStatus.NOT_FOUND);
        }
        try {
            Food newFood = new Food();

            newFood.setName(foodDto.getName());
            newFood.setDescription(foodDto.getDescription());
            newFood.setPrice(foodDto.getPrice());
            newFood.setImage(imageOptional.get());
            newFood.setCategory(categoryOptional.get());
            newFood.setAvailable(foodDto.isAvailable());
//            newFood.setAvailableFrom(foodDto.getAvailableFrom());
//            newFood.setAvailableTo(foodDto.getAvailableTo());
//            newFood.setPreparationTime(foodDto.getPreparationTime());

            foodRepo.save(newFood);

            return new ResponseEntity<>(new ApiResponse("New product added!", true, newFood), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ApiResponse("An error occurred while adding the product!", false), HttpStatus.EXPECTATION_FAILED);
        }
    }

    public HttpEntity<?> editFood(UUID foodId, FoodDto foodDto) {
        Optional<Food> optionalFood = foodRepo.findById(foodId);
        if (optionalFood.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("Product not found!", false), HttpStatus.NOT_FOUND);
        }
        Optional<Attachment> foodImg = attachmentRepo.findById(foodDto.getImageId());
        if (foodImg.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("Image not found!", false), HttpStatus.NOT_FOUND);
        }
        Optional<Category> categoryOptional = categoryRepo.findById(foodDto.getCategoryId());
        if (categoryOptional.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("Category not found!", false), HttpStatus.NOT_FOUND);
        }
        try {
            Food editFood = optionalFood.get();

            editFood.setName(foodDto.getName());
            editFood.setDescription(foodDto.getDescription());
            editFood.setPrice(foodDto.getPrice());
            editFood.setAvailable(foodDto.isAvailable());
//            editFood.setAvailableFrom(foodDto.getAvailableFrom());
//            editFood.setAvailableTo(foodDto.getAvailableTo());
//            editFood.setPreparationTime(foodDto.getPreparationTime());
            editFood.setImage(foodImg.get());
            editFood.setCategory(categoryOptional.get());

            foodRepo.save(editFood);

            return new ResponseEntity<>(new ApiResponse("Product success edited!", true), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ApiResponse("An error occurred while editing the product!", false), HttpStatus.EXPECTATION_FAILED);
        }
    }

    public HttpEntity<?> deleteFood(UUID foodId) {
        Optional<Food> optionalFood = foodRepo.findById(foodId);
        if (optionalFood.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("Product not found!", false), HttpStatus.NOT_FOUND);
        }
        try {
            foodRepo.delete(optionalFood.get());
            return new ResponseEntity<>(new ApiResponse("Deleted success!", true), HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(new ApiResponse("An error occurred while deleting the product!", false), HttpStatus.EXPECTATION_FAILED);
        }
    }
}
