package com.example.food_delivery_app.service;

import com.example.food_delivery_app.common.ApiResponse;
import com.example.food_delivery_app.dtos.PermissionDto;
import com.example.food_delivery_app.entity.permission.Permission;
import com.example.food_delivery_app.repository.PermissionRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PermissionService {
    private final PermissionRepo permissionRepo;

    public HttpEntity<?> getAllPermission(){
        List<Permission> allPermission = findAllPermission();
        if (allPermission.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("Permissions not found!", false), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new ApiResponse("Permissions found!", true, allPermission), HttpStatus.OK);
    }

    public HttpEntity<?> getPermissionById(UUID permissionId){
        Optional<Permission> permissionById = findPermissionById(permissionId);
        if (permissionById.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("Permission not found!", false), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new ApiResponse("Permission found!", true, permissionById.get()), HttpStatus.OK);
    }

//    public HttpEntity<?> addPermission(PermissionDto permissionDto){
//
//    }

    private List<Permission> findAllPermission(){
        return permissionRepo.findAll();
    }

    private Optional<Permission> findPermissionById(UUID permissionId){
        return permissionRepo.findById(permissionId);
    }
}
