package com.example.food_delivery_app.service;

import com.example.food_delivery_app.dtos.OrdersDto;
import com.example.food_delivery_app.entity.order.Order;
import com.example.food_delivery_app.entity.user.User;
import com.example.food_delivery_app.common.ApiResponse;
import com.example.food_delivery_app.repository.AddressRepo;
import com.example.food_delivery_app.repository.OrderRepo;
import com.example.food_delivery_app.repository.UserRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepo orderRepo;
    private final AddressRepo addressRepo;
    private final UserRepo userRepo;

    public HttpEntity<?> getAllOrders(){
        List<Order> allOrders = findAllOrders();
        if (allOrders.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("Orders not found!", false), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new ApiResponse("Orders found!", true, allOrders), HttpStatus.OK);
    }

    public HttpEntity<?> getOrderById(UUID orderId){
        Optional<Order> optionalOrder = findById(orderId);
        if (optionalOrder.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("Order not found", false), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new ApiResponse("Order found!", true, optionalOrder.get()), HttpStatus.OK);
    }

    // TODO: 15.06.2022 Add, edit delete tugatish!
//    public HttpEntity<?> addOrder(OrdersDto ordersDto){
//
//        try {
//            Order newOrder = new Order();
//        }
//    }

    private Optional<User> findByUserId(UUID userId){
        return userRepo.findById(userId);
    }

    private List<Order> findAllOrders(){
        return orderRepo.findAll();
    }

    private Optional<Order> findById(UUID orderId){
        return orderRepo.findById(orderId);
    }
}
