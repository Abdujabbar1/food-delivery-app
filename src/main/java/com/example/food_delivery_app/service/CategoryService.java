package com.example.food_delivery_app.service;

import com.example.food_delivery_app.entity.category.Category;
import com.example.food_delivery_app.common.ApiResponse;
import com.example.food_delivery_app.repository.CategoryRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepo categoryRepo;

    public HttpEntity<?> getAllCategory(){
        List<Category> allCategory = categoryRepo.findAll();
        if (allCategory.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("Category not found!", false), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new ApiResponse("All Categories!", true, allCategory), HttpStatus.OK);
    }

    public HttpEntity<?> getCategoryById(UUID categoryId){
        Optional<Category> optionalCategory = categoryRepo.findById(categoryId);
        if (optionalCategory.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("Category not found!", false), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new ApiResponse("Category found!", true), HttpStatus.OK);
    }

   // public HttpEntity<?> editCategory(UUID categoryId, Category)
}
