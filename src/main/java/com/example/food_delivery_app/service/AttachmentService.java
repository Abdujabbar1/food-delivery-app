package com.example.food_delivery_app.service;

import com.example.food_delivery_app.entity.attachment.Attachment;
import com.example.food_delivery_app.entity.attachment.AttachmentContent;
import com.example.food_delivery_app.common.ApiResponse;
import com.example.food_delivery_app.repository.AttachmentContentRepo;
import com.example.food_delivery_app.repository.AttachmentRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AttachmentService {
    private final AttachmentRepo attachmentRepo;
    private final AttachmentContentRepo contentRepo;

    public HttpEntity<?> fileUpload(MultipartFile file) {
        try {
            Attachment attachmentSave = attachmentRepo.save(new Attachment(file.getOriginalFilename(),
                    file.getContentType(),
                    file.getSize()));
            contentRepo.save(new AttachmentContent(file.getBytes(), attachmentSave));
            return new ResponseEntity<>(new ApiResponse("File success added!", true), HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity<>(new ApiResponse("An error occurred while adding the file", false), HttpStatus.EXPECTATION_FAILED);
        }
    }

    public HttpEntity<?> deleteFile(UUID attachmentId){
        Optional<Attachment> optionalAttachmentById = attachmentRepo.findById(attachmentId);
        if (optionalAttachmentById.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("File not found!", false), HttpStatus.NOT_FOUND);
        }
        AttachmentContent contentById = contentRepo.getById(attachmentId);
        contentRepo.deleteById(contentById.getId());
        attachmentRepo.delete(optionalAttachmentById.get());
        return new ResponseEntity<>(new ApiResponse("File Success deleted!", false), HttpStatus.OK);
    }
}
