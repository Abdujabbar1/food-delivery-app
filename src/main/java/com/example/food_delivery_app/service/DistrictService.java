package com.example.food_delivery_app.service;

import com.example.food_delivery_app.dtos.DistrictDto;
import com.example.food_delivery_app.entity.address.District;
import com.example.food_delivery_app.common.ApiResponse;
import com.example.food_delivery_app.repository.DistrictRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DistrictService {

    public final DistrictRepo districtRepo;

    public HttpEntity<?> getAllDistrict() {
        List<District> allDistrict = findAllDistrict();
        if (allDistrict.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("District not found!", false), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new ApiResponse("District found!", true, allDistrict), HttpStatus.OK);
    }

    public HttpEntity<?> getDistrictById(UUID districtId) {
        Optional<District> optionalDistrict = findDistrictById(districtId);
        if (optionalDistrict.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("District not found!", false), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new ApiResponse("District found!", true, optionalDistrict.get()), HttpStatus.OK);
    }

    public HttpEntity<?> addDistrict(DistrictDto districtDto) {
        if (existByName(districtDto.getName())) {
            return new ResponseEntity<>(new ApiResponse("A district with that name already exists!", false), HttpStatus.ALREADY_REPORTED);
        }
        try {
            District newDistrict = new District();

            newDistrict.setName(districtDto.getName());

            districtRepo.save(newDistrict);

            return new ResponseEntity<>(new ApiResponse("District Added!", true, newDistrict), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ApiResponse("An error occurred while adding the district!", false), HttpStatus.EXPECTATION_FAILED);
        }
    }

    public HttpEntity<?> editDistrict(UUID districtId,DistrictDto districtDto) {

        Optional<District> districtById = findDistrictById(districtId);
        if (districtById.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("District not found!", false), HttpStatus.NOT_FOUND);
        }

        if (existByName(districtDto.getName())) {
            return new ResponseEntity<>(new ApiResponse("A district with that name already exists!", false), HttpStatus.ALREADY_REPORTED);
        }

        try {
            District editDistrict = districtById.get();

            editDistrict.setName(districtDto.getName());

            districtRepo.save(editDistrict);

            return new ResponseEntity<>(new ApiResponse("District edited!", true, editDistrict), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ApiResponse("An error occurred while adding the district!", false), HttpStatus.EXPECTATION_FAILED);
        }
    }

    public HttpEntity<?> deleteDistrict(UUID districtId){
        Optional<District> districtById = findDistrictById(districtId);
        if (districtById.isEmpty()){
            return new ResponseEntity<>(new ApiResponse("District not found!", false), HttpStatus.NOT_FOUND);
        }
        try {
            districtRepo.delete(districtById.get());

            return new ResponseEntity<>(new ApiResponse("District deleted!", true), HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(new ApiResponse("An error occurred while deleting the district!", false), HttpStatus.EXPECTATION_FAILED);
        }
    }

    private boolean existByName(String districtName){
        return districtRepo.existsByName(districtName);
    }
    private Optional<District> findDistrictById(UUID districtId){
        return districtRepo.findById(districtId);
    }

    private List<District> findAllDistrict(){
        return districtRepo.findAll();
    }
}
