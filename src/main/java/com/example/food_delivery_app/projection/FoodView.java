package com.example.food_delivery_app.projection;


import com.example.food_delivery_app.entity.food.Food;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;


public interface FoodView {

    UUID getId();

    String getName();

    String getDescription();

    double getPrice();

    UUID getImageId();

}
