package com.example.food_delivery_app.repository;

import com.example.food_delivery_app.entity.address.Address;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AddressRepo extends JpaRepository<Address, UUID> {
}
