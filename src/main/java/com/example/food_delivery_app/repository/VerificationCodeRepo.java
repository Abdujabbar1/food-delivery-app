package com.example.food_delivery_app.repository;

import com.example.food_delivery_app.entity.verificationCode.VerificationCodes;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface VerificationCodeRepo extends JpaRepository<VerificationCodes, UUID> {
}
