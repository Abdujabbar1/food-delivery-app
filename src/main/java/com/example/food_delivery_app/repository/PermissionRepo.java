package com.example.food_delivery_app.repository;

import com.example.food_delivery_app.entity.permission.Permission;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PermissionRepo extends JpaRepository<Permission, UUID> {
}
