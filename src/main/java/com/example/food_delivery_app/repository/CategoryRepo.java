package com.example.food_delivery_app.repository;

import com.example.food_delivery_app.entity.category.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CategoryRepo extends JpaRepository<Category, UUID> {
}
