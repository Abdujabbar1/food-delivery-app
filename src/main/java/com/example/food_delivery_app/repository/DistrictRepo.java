package com.example.food_delivery_app.repository;

import com.example.food_delivery_app.entity.address.District;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface DistrictRepo extends JpaRepository<District, UUID> {

    boolean existsByName(String districtName);
}
