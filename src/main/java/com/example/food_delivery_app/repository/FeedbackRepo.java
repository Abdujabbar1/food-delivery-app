package com.example.food_delivery_app.repository;

import com.example.food_delivery_app.entity.feedback.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface FeedbackRepo extends JpaRepository<Feedback, UUID> {
}
