package com.example.food_delivery_app.repository;

import com.example.food_delivery_app.entity.siteInfo.SiteInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SiteInfoRepo extends JpaRepository<SiteInfo, UUID> {
}
