package com.example.food_delivery_app.repository;

import com.example.food_delivery_app.entity.attachment.AttachmentContent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AttachmentContentRepo extends JpaRepository<AttachmentContent, UUID> {

}
