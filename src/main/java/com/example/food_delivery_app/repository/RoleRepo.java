package com.example.food_delivery_app.repository;

import com.example.food_delivery_app.entity.role.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface RoleRepo extends JpaRepository<Role, UUID> {
}
