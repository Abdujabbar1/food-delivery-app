package com.example.food_delivery_app.repository;

import com.example.food_delivery_app.entity.food.Food;
import com.example.food_delivery_app.projection.FoodView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface FoodRepo extends JpaRepository<Food, UUID> {
    boolean existsByName(String foodName);


    @Query(nativeQuery = true, value = "select cast(f.id as varchar) as id, \n" +
            "       f.name as name,\n" +
            "       f.description as description,\n" +
            "       f.price as price,\n            " +
            "       cast(a.id as varchar) as imageId\n" +
            "from foods as f\n" +
            "join attachments a on a.id = f.image_id")
    List<FoodView> getAllFoods();

    @Query(nativeQuery = true, value = "select cast(f.id as varchar) as id,\n" +
            "       f.name                as name,\n" +
            "       f.description         as description,\n" +
            "       f.price               as price,\n" +
            "       cast(a.id as varchar) as imageId\n" +
            "from foods as f\n" +
            "         join attachments a on a.id = f.image_id\n" +
            "where f.id =:foodId")
    FoodView getFoodById(UUID foodId);
}
