package com.example.food_delivery_app.repository;

import com.example.food_delivery_app.entity.attachment.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AttachmentRepo extends JpaRepository<Attachment, UUID> {
}
