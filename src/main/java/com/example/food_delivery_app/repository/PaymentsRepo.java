package com.example.food_delivery_app.repository;

import com.example.food_delivery_app.entity.paytype.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PaymentsRepo extends JpaRepository<Payment, UUID> {
}
