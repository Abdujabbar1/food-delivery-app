package com.example.food_delivery_app.repository;

import com.example.food_delivery_app.entity.delivery.DeliveryPricing;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface DeliveryPricingRepo extends JpaRepository<DeliveryPricing, UUID> {
}
